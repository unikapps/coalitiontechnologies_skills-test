<products>
        <product>
            <product_name>{{  $data['product_name'] }}</product_name>
            <quantity_in_stock>{{ $data['quantity_in_stock'] }}</quantity_in_stock>
            <price_per_item>{{ $data['price_per_item'] }}</price_per_item>
        </product>
</products>