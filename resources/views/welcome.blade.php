<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style>
        .spacer {
            margin-top: 100px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">

        <div class="spacer"></div>

        <div class="col-sm-10 col-sm-offset-2 col-xs-12">
            <form method="POST" class="form-inline">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="sr-only" for="product_name">Product Name</label>
                    <input type="text" name="product_name" class="form-control" id="product_name"
                           placeholder="Product Name">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="quantity_in_stock">Quantity in stock</label>
                    <input type="text" name="quantity_in_stock" class="form-control" id="quantity_in_stock"
                           placeholder="Quantity in stock">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="price_per_item">Price per item</label>
                    <input type="text" name="price_per_item" class="form-control" id="price_per_item"
                           placeholder="Price per item">
                </div>


                <button type="submit" class="btn btn-default">Submit</button>

            </form>

            @if(isset($data) && !empty($data))
                <br>
                <br>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Product name</th>
                        <th>Quantity in stock</th>
                        <th>Price per item</th>
                        <th>Datetime submitted</th>
                        <th>Total value number</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>{{ $data['product_name'] }}</td>
                        <td>{{ $data['quantity_in_stock'] or 0 }}</td>
                        <td>{{ $data['price_per_item'] or 0 }}</td>
                        <td>{{ $data['datetime_submitted'] or date('Y-m-d H:i:s') }}</td>
                        <td>{{ ($data['quantity_in_stock'] * $data['price_per_item']) or 0 }}</td>
                    </tr>
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
</body>
</html>
