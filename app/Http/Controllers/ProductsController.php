<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller
{

    /**
     * @TODO: Validate form
     * @Todo: submit with ajax
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function store(Request $request)
    {

        $data = [];
        if ($request->isMethod('POST'))
        {

            //Form Data
            $data = [
                'product_name' => $request->product_name,
                'quantity_in_stock' => $request->quantity_in_stock,
                'price_per_item' => $request->quantity_in_stock,
                'datetime_submitted' => Carbon::now()
            ];

            // output form data to xml file
            $this->to_xml($data);
        }

        $collect_data = collect($data);

        $sum = $collect_data->sum('quantity_in_stock') * $collect_data->sum('price_per_item');

        return view('welcome', compact('data', 'sum'));
    }

    /**
     * @param array $data
     * @return mixed output xml file to storage directory
     */
    private function to_xml($data){
        $content = View::make('xml', $data)->render();
        File::put(storage_path().'/products.xml', $content);
        return Response::make($content, 200)->header('Content-Type', 'application/xml');
    }
}
